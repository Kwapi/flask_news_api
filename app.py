import requests
import dateutil.parser
import pytz

from flask import Flask, render_template, request, flash, redirect, url_for
from flask_paginate import Pagination, get_page_parameter

app = Flask(__name__)
app.secret_key = "my secret key"
app.config['DEBUG'] = True


@app.template_filter('my_date_time_format')
def format_datetime(value, date_time_format='%d-%m-%Y %H:%M:%S'):
    if value is None:
        return ""
    my_tz = pytz.timezone('Europe/Warsaw')
    parsed_obj = dateutil.parser.isoparse(value)
    my_date_time = parsed_obj.astimezone(my_tz)
    final_datetime = my_date_time.strftime(date_time_format)
    return final_datetime


g_q = ''
g_qInTitle = ''
g_from_date = ''
g_to_date = ''
g_language = ''
g_sortBy = ''
g_pageSize = ''
g_category = ''


@app.route('/', methods=['GET'])
def index():
    global g_q, g_qInTitle, g_from_date, g_to_date, g_language, g_sortBy, g_pageSize, g_category
    g_q, g_qInTitle, g_from_date, g_to_date, g_language, g_sortBy, g_pageSize, g_category = '', '', '', '', '', '', '', ''
    return render_template('index.html')


@app.route('/top_headlines/', methods=['GET', 'POST'])
def top_headlines():
    global g_language, g_category, g_q, g_pageSize
    country = request.form.get('country')
    if country:
        g_language = country
    category = request.form.get('category')
    if category:
        g_category = category
    q = request.form.get('q')
    if q:
        g_q = q
    page_size = request.form.get('pageSize')
    if page_size:
        g_pageSize = page_size
    page = request.args.get(get_page_parameter(), type=int, default=1)
    response = requests.get('http://newsapi.org/v2/top-headlines?apiKey=a56afcb43eb64a0db54a6675ed1bc260',
                            params={'country': g_language, 'category': g_category,
                                    'q': g_q, 'pageSize': g_pageSize, 'page': page})
    data = response.json()
    total = data.get('totalResults')
    news_on_page = int(g_pageSize)
    pagination = Pagination(total=total,
                            record_name='news',
                            per_page=news_on_page,
                            css_framework='foundation',
                            page=page)
    return render_template('top_headlines.html', data=data, pagination=pagination)


@app.route('/everything/', methods=['GET', 'POST'])
def everything():
    global g_q, g_qInTitle, g_from_date, g_to_date, g_language, g_sortBy, g_pageSize
    q = request.form.get('q')
    if q:
        g_q = q
    qInTitle = request.form.get('qInTitle')
    if qInTitle:
        g_qInTitle = qInTitle
    from_date = request.form.get('from')
    if from_date:
        g_from_date = from_date
    to_date = request.form.get('to')
    if to_date:
        g_to_date = to_date
    language = request.form.get('language')
    if language:
        g_language = language
    sortBy = request.form.get('sortBy')
    if sortBy:
        g_sortBy = sortBy
    pageSize = request.form.get('pageSize')
    if pageSize:
        g_pageSize = pageSize
    page = request.args.get(get_page_parameter(), type=int, default=1)
    response = requests.get(f'http://newsapi.org/v2/everything?apiKey=a56afcb43eb64a0db54a6675ed1bc260',
                            params={'q': g_q, 'qInTitle': g_qInTitle,
                                    'from': g_from_date, 'to': g_to_date, 'language': g_language,
                                    'sortBy': g_sortBy, 'pageSize': g_pageSize, 'page': page})
    data = response.json()
    news_on_page = int(g_pageSize)
    total = data.get('totalResults')

    if request.method == 'POST':
        print("Request method == POST")
        if not request.form.get('q') and not request.form.get('qInTitle'):
            flash("Hey Guy! You have to specify a keyword in article body or title")
            return redirect(url_for('index'))

    pagination = Pagination(total=total,
                            record_name='news',
                            per_page=news_on_page,
                            css_framework='foundation',
                            page=page)
    return render_template('everything.html', data=data, pagination=pagination)
